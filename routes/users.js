var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var connection = mysql.createConnection({ host: 'localhost', user: 'root', password: '', database: 'boney' });

router.post('/login', function(req, res) {
	console.log(req.body);
	var user = req.body.username
	var pass = req.body.password
	connection.query('SELECT * FROM users WHERE username = ? AND password = ?', [user, pass], function(err, rows) {
		if (err) {
			res.json(err);
		} else {
			if (rows.length)
				res.json({ error: false, user: rows[0] });
			else
				res.json({ error: true });
		}
	});
});

router.post('/create', function(req, res) {
	var name = req.body.name;
	var last_name = req.body.last_name;
	var username = req.body.username;
	var email = req.body.email;
	var password = req.body.password;
	connection.query('INSERT INTO users VALUES(NULL, ?, ?, ?, ?, ?, 0)', [name, last_name, username, email, password], function(err, rows) {
		if (err) {
			res.json({ success: false, service: 'users/create', err: err});
		} else {
			connection.query('SELECT * FROM users WHERE username = ? AND password = ?', [username, password], function(err, rows) {
			if (err)
				res.json({ success: false, service: 'users/create', err: err });
			else {
				if (rows.length)
					res.json({ success: true, service: 'users/create', data: rows[0] });
				else
					res.json({ success: false, service: 'users/create', err: {} });
			}
		});
		}
	});
});

router.put('/status', function(req, res) {
	var id = req.body.id;
	var status = req.body.status;
	connection.query('UPDATE users SET status = ? WHERE id = ?', [status, id], function(err, rows) {
		if (err)
			res.json({ success: false, service: 'users/status', err: err});
		else
			res.json({ success: true, service: 'users/status', data: { status: status } });
	});
});

router.delete('/sing_out', function(req, res) {
	res.json({success:true, data: {message: 'bye bye'}});
});

module.exports = router;