var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var connection = mysql.createConnection({ host: 'localhost', user: 'root', password: '', database: 'boney' });

router.get('/', function(req, res, next) {
  	res.render('index', { title: 'Express' });
});

router.get('/patients/:user', function(req, res) {
	var userID = req.params.user;
	console.log(JSON.stringify(req.headers));
	console.log(userID);

	var data = { rows: [] };

	var finishRequest = function() {
		res.json({ success: true, service: 'patients', data: data });
	};

	var lookForLocations = function(user, last) {
		connection.query('SELECT l.latitude lat, l.longitude, ubication FROM locations l LEFT JOIN places p ON sqrt(pow(l.latitude - p.latitude, 2) + pow(l.longitude - p.longitude, 2)) < 0.1 GROUP BY p.place_id', [user.id], function(err, rows) {
			if (err && !rows.length)
				res.json({ success: false, service: 'locations/user', err: err });
			else {
				user.locations = rows;
				data.rows.push(user);
				if (last)
					finishRequest();
			}
		});
	};

	connection.query('SELECT id, name, last_name FROM patients INNER JOIN users ON patient_id = id WHERE keeper_id = ?', [userID], function(err, rows) {
		if (err && !rows.length)
			res.json({ success: false, service: 'patients', err: err });
		else {
			for (var i = 0; i < rows.length; i++)
				lookForLocations(rows[i], i == rows.length - 1);
		}
	});
});

router.get('/locations/:user', function(req, res) {
	var userID = req.params.user;
	connection.query('SELECT l.user_id, l.latitude, l.longitude, ubication FROM locations l LEFT JOIN places p ON sqrt(pow(l.latitude - p.latitude, 2) + pow(l.longitude - p.longitude, 2)) < 0.1 GROUP BY p.place_id', [userID], function(err, rows) {
		if (err && !rows.length) {
			res.json({ success: false, service: 'locations/user', err: err });
		} else {
			res.json({ success: true, service: 'locations/user', data: { rows: rows} })
		}
	});
});

router.get('/patient/:id', function(req, res) {
	
})

router.post('/login', function(req, res) {
	var user = req.body.username
	var pass = req.body.password
	connection.query('SELECT * FROM users WHERE username = ? AND password = ?', [user, pass], function(err, rows) {
		if (err) {
			res.json({ success: false, service: 'login', err: err });
		} else {
			if (rows.length)
				res.json({ success: true, service: 'login', data: rows[0]} );
			else
				res.json({ success: false, service: 'login', err: {} });
		}
	});
});

router.post('/locations', function(req, res) {
	var userID = req.body.user_id;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	connection.query('INSERT INTO locations VALUES(?, ?, ?, date(now()))', [userID, latitude, longitude], function(err, rows) {
		if (err) {
			res.json({ success: false, service: 'locations', err: err });
		} else {
			res.json({ success: true, service: 'locations', data: {} })
		}
	});
});

module.exports = router;
